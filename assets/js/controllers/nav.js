function drncdNavBarLv3MenuContainer(i, j) {
    var _id = "menu_" + i + "_" + j + "_container";
    var _divContainer = "<div class=\'menulv3_container\' id=\'" + _id + "\'></div>";
    $(_divContainer).appendTo($("#submenuLevel3"));
    var _sdivContainer = $("#" + _id);

    // Table
    var _tid = _id + "_table";
    var _tableContainer = "<table id=\'" + _tid + "\'></table>";
    $(_tableContainer).appendTo(_sdivContainer);

    // First Line
    var _tr0id = _tid + "_tr0";
    var _tr0Container = "<tr id=\'" + _tr0id + "\'></tr>";
    $(_tr0Container).appendTo("#" + _tid);
    for ( var i = 0; i < 4; ++i ) {
        var _tdid = _tr0id + "_" + i;
        var _tdContainer = "<td id=\'" + _tdid + "\'></td>";
        $(_tdContainer).appendTo("#" + _tr0id);
    }

    // Second Line
    var _tr1id = _tid + "_tr1";
    var _tr1Container = "<tr id=\'" + _tr1id + "\'></tr>";
    $(_tr1Container).appendTo("#" + _tid);
    for ( var i = 0; i < 4; ++i ) {
        var _tdid = _tr1id + "_" + i;
        var _tdContainer = "<td id=\'" + _tdid + "\'></td>";
        $(_tdContainer).appendTo("#" + _tr1id);
    }
    return _id;
}

var __visiableLv3Items = {};

function drncdNavBarAddMenu() {
    var _mainMenu = $("#drncdMainMenu");
    var _lv1Count = drncd_data_menu.length;
    for ( var i = 0; i < _lv1Count; ++i ) {
        var _lv1Item = drncd_data_menu[i];
        var _lv1ItemBaseId = "menu_" + i;
        var _a = "<a href=\'" + _lv1Item.url + "\'";
        if ( _lv1Item.target != "" ) {
            _a += (" target=" + _lv1Item.target);
        }
        _a += (">" + _lv1Item.item + "</a>");
        var _li = "<li id=\'" + _lv1ItemBaseId + "\'>" + _a + "</li>";
        $(_li).appendTo(_mainMenu);

        // Check Lv2 
        if ( _lv1Item.children === undefined ) continue;
        if ( _lv1Item.children.length == 0 ) continue;
        var _lv2ItemPanelId = _lv1ItemBaseId + "_panel";
        var _lv2ItemPanel = "<ul class=\'menulv2\' id=\'" + _lv2ItemPanelId + "\'>";
        var _lv2Count = _lv1Item.children.length;
        for ( var j = 0; j < _lv2Count; ++j ) {
            var _lv2ItemBaseId = _lv1ItemBaseId + "_" + j;
            var _lv2Item = _lv1Item.children[j];
            var _lv2ItemLi = "<li id=\'" + _lv2ItemBaseId + "\'>" + 
                             "<a href=\'" + _lv2Item.url + "\'";
            if ( _lv2Item.target != "" ) {
                _lv2ItemLi += (" target=" + _lv2Item.target);
            }
            _lv2ItemLi += ">" + _lv2Item.item + "</a></li>";
            _lv2ItemPanel += _lv2ItemLi;

            // Check Lv2 Item has image or Lv3 
            do {
                if ( _lv2Item.image === undefined ) break;
                if ( _lv2Item.image == "" ) break;
                if ( _lv2Item.image == null ) break;
                var _lv2ItemImgId = _lv2ItemBaseId + "_image";
                var _lv2ItemImg = "<img id=\'" + _lv2ItemImgId + "\' " + 
                                  "class=\'menulv2img\' src=\'" + 
                                  _lv2Item.image + "\' />";
                $(_lv2ItemImg).appendTo($("#submenuLevel3"));
                $("#" + _lv2ItemImgId).css("display", "none");

                __visiableLv3Items[_lv2ItemBaseId] = _lv2ItemImgId;
            } while( false );

            // Check has Lv3 
            do {
                if ( _lv2Item.children === undefined ) break;
                if ( _lv2Item.children == null ) break;
                if ( _lv2Item.children.length == 0 ) break;
                var _lv3Count = _lv2Item.children.length;

                // Create lv3 menu container 
                var _lv3ContainerId = drncdNavBarLv3MenuContainer(i, j);

                for ( var k = 0; k < _lv3Count; ++k ) {
                    var _lv3CellCtntId = _lv3ContainerId + "_table_tr" + (k % 2) + 
                                         "_" + Math.floor(k / 2);
                    var _lv3Item = _lv2Item.children[k];
                    var _lv3InnerCtnt = "";
                    if ( 
                        _lv3Item.image === undefined || 
                        _lv3Item.image == null ||
                        _lv3Item.image == ""
                        ) {
                        _lv3InnerCtnt = "<span>" + _lv3Item.item + "</span>";
                    } else {
                        _lv3InnerCtnt = "<img src=\'" + _lv3Item.image + "\' />";
                    }
                    var _lv3ItemBaseId = _lv2ItemBaseId + "_" + k;
                    var _lv3ItemDiv = "<div><div><a href=\'" + _lv3Item.url + 
                                      "\'>" + _lv3Item.item + "</a></div>" + 
                                      _lv3InnerCtnt + "</div>";
                    $(_lv3ItemDiv).appendTo($("#" + _lv3CellCtntId));

                    // Add Mouse Over function 
                    $("#" + _lv3CellCtntId).mouseenter(function() {
                        var _id = $(this).attr("id");
                        $("#" + _id + " > div > div").css("display", "block");
                    }).mouseleave(function() {
                        var _id = $(this).attr("id");
                        $("#" + _id + " > div > div").css("display", "none");
                    });
                }
                __visiableLv3Items[_lv2ItemBaseId] = _lv3ContainerId;
            } while ( false );
        }
        _lv2ItemPanel += "</ul>";
        $(_lv2ItemPanel).appendTo($("#submenuLevel2"));
        $("#" + _lv2ItemPanelId).addClass("menulv2_" + _lv2Count);
        $("#" + _lv2ItemPanelId).css("display", "none");
        $("#" + _lv2ItemPanelId + " li").mouseenter(function() {
            var _id = $(this).attr("id");
            $.each(__visiableLv3Items, function(key, value) {
                if ( _id == key ) {
                    $("#" + value).css("display", "block");
                } else {
                    $("#" + value).css("display", "none");
                }
            });
        });
    }
}

var g_mouseInMenuArea = false;
var g_mouseCheckingTimeout = null;
var g_currentObjectMouseIn = null;
var g_currentDisplayedSubmenu = null;

function drncdNavBarInitialize() {
    //$('body').css("color", "#515251");
    $('#drncd_login').click(function() {
        $('#login_dialog').modal('show');
    });

    // Initialize the menu
    drncdNavBarAddMenu();

    // Move indicator to Menu(About)
    var _firstMenuItem = $('#drncdMainMenu li:eq(0)');
    var _menuIndicator = $('#menuIndicator');
    var _m1stCenter = _firstMenuItem.position().left + _firstMenuItem.width() / 2;
    var _miLeft = _m1stCenter - _menuIndicator.width() / 2;
    _menuIndicator.css("margin-left", _miLeft);

    $('#drncdMainMenu li').mouseenter(function() {
        var _c = $(this).position().left + $(this).width() / 2;
        var _il = _c - _menuIndicator.width() / 2;
        _menuIndicator.animate({marginLeft: _il}, 100);

        var _mid = $(this).attr("id").substr(5);
        if ( drncd_data_menu[_mid].children === undefined ) {
            // No child, should hide
            return;
        }

        if ( !g_mouseInMenuArea ) $('#submenuPanel').fadeIn(300);   

        g_mouseInMenuArea = true;
        g_currentObjectMouseIn = $(this).attr('id');
        // Change to display the sub menu item
        
        var _lv1ItemId = $(this).attr("id");
        if ( _lv1ItemId != g_currentDisplayedSubmenu && g_currentDisplayedSubmenu != null ) {
            var _submenuPanel = g_currentDisplayedSubmenu + "_panel";
            $("#" + _submenuPanel).css("display", "none");
        }
        $("#" + _lv1ItemId + "_panel").css("display", "block");
        // Select the first item in the panel 
        var _firstItemId = _lv1ItemId + "_0";
        g_currentDisplayedSubmenu = _lv1ItemId;
        $.each(__visiableLv3Items, function(key, value) {
            if ( _firstItemId == key ) {
                $("#" + value).css("display", "block");
            } else {
                $("#" + value).css("display", "none");
            }
        });
    }).mouseleave(function() {
        if ( $(this).attr('id') != g_currentObjectMouseIn ) {
            return;
        }
        g_mouseInMenuArea = false;
        g_currentObjectMouseIn = null;

        if ( g_mouseCheckingTimeout != null ) clearTimeout(g_mouseCheckingTimeout);
        g_mouseCheckingTimeout = setTimeout(function() {
            if ( g_mouseInMenuArea == false ) $('#submenuPanel').fadeOut(300);
        }, 150);
    });

    $('#submenuPanel').mouseenter(function() {
        g_mouseInMenuArea = true;
        g_currentObjectMouseIn = $(this).attr('id');
    }).mouseleave(function() {
        if ( $(this).attr('id') != g_currentObjectMouseIn ) {
            return;
        }
        g_mouseInMenuArea = false;
        g_currentObjectMouseIn = null;

        if ( g_mouseCheckingTimeout != null ) clearTimeout(g_mouseCheckingTimeout);
        g_mouseCheckingTimeout = setTimeout(function() {
            if ( g_mouseInMenuArea == false ) $('#submenuPanel').fadeOut(300);
        }, 150);
    });
}
