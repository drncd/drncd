function drncdFooterInitialize() {
    $("#footer_csnumber").text("客服电话: " + drncd_data_footer["custom-service-tel"]);
    $("#footer_hlnumber").text("热线电话: " + drncd_data_footer["hot-line"]);
    $("#footer_icp").text(drncd_data_footer["icp"]);

    // Change social accounts
    var _saCount = drncd_data_footer["social-accounts"].length;
    for ( var i = 0; i < _saCount; ++i ) {
        var _sa = drncd_data_footer["social-accounts"][i];
        var _socialDiv = "<div class=\'col-lg-1 col-xs-2 col-md-1\'>" + 
                         "<a href=\'" + _sa.link + "\'>" + 
                         "<img src=\'" + _sa.icon + "\' />" + 
                         "</a></div>";
        $(_socialDiv).appendTo($("#footerSocialPanel"));
    }
}