var drncdPostDefaultBackgroundColor = "#E095B0";

function drncdPostGroupCreateTopPost( news_data ) {
    var layoutDiv = $("<div class=\'col-lg-12 col-md-12 col-sm-12 col-xs-12\' />");

    var linkObj = $("<a />");
    linkObj.attr({ href : news_data.url });

    var innerLayoutDiv = $("<div class=\'news-top row img-hover\' />");

    var textDiv = $("<div class=\'col-lg-3 col-md-3 col-sm-6 col-xs-6 news-text\' />");
    var dateDiv = $("<div class=\'news-date\'>" + news_data.post_date + "</div>");
    if ( 
        news_data.date_color !== undefined && 
        news_data.date_color != null &&
        news_data.date_color != ""
        ) {
        dateDiv.css("color", news_data.date_color);
    }
    dateDiv.appendTo(textDiv);
    var titleDiv = $("<div class=\'news-title\'>" + news_data.post_title + "</div>");
    if ( 
        news_data.title_color !== undefined && 
        news_data.title_color != null &&
        news_data.title_color != ""
        ) {
        titleDiv.css("color", news_data.title_color);
    }
    titleDiv.appendTo(textDiv);
    if ( news_data.background_color !== undefined && news_data.background_color != null && news_data.background_color != "" ) {
        textDiv.css("background-color", news_data.background_color);
    } else {
        textDiv.css("background-color", drncdPostDefaultBackgroundColor);
    }
    textDiv.appendTo(innerLayoutDiv);

    var imgDiv = $("<div class=\'col-lg-9 col-md-9 col-sm-6 col-xs-6 news-image\' />");
    console.log("promote_image: " + news_data.promote_image);
    imgDiv.css("background-image", "url(\'" + news_data.promote_image + "\')");
    imgDiv.appendTo(innerLayoutDiv);
    innerLayoutDiv.appendTo(linkObj);
    linkObj.appendTo(layoutDiv);

    return layoutDiv;
}
function drncdPostGroupCreateImagePost( news_data ) {
    var layoutDiv = $("<div class=\'col-lg-6 col-md-6 col-sm-12 col-xs-12\' />");

    var linkObj = $("<a />");
    linkObj.attr({ href : news_data.url });

    var innerLayoutDiv = $("<div class=\'news row\' />");

    var textDiv = $("<div class=\'col-lg-6 col-md-6 col-sm-6 col-xs-6 news-text\' />");
    var dateDiv = $("<div class=\'news-date\'>" + news_data.post_date + "</div>");
    if ( 
        news_data.date_color !== undefined && 
        news_data.date_color != null &&
        news_data.date_color != ""
        ) {
        dateDiv.css("color", news_data.date_color);
    }
    dateDiv.appendTo(textDiv);
    var titleDiv = $("<div class=\'news-title\'>" + news_data.post_title + "</div>");
    if ( 
        news_data.title_color !== undefined && 
        news_data.title_color != null &&
        news_data.title_color != ""
        ) {
        titleDiv.css("color", news_data.title_color);
    }
    titleDiv.appendTo(textDiv);
    if ( news_data.background_color !== undefined && news_data.background_color != null && news_data.background_color != "" ) {
        textDiv.css("background-color", news_data.background_color);
    } else {
        textDiv.css("background-color", drncdPostDefaultBackgroundColor);
    }
    textDiv.appendTo(innerLayoutDiv);

    var imgDiv = $("<div class=\'col-lg-6 col-md-6 col-sm-6 col-xs-6 news-image\' />");
    imgDiv.css("background-image", "url(\'" + news_data.promote_image + "\')");
    imgDiv.appendTo(innerLayoutDiv);

    innerLayoutDiv.appendTo(linkObj);
    linkObj.appendTo(layoutDiv);

    return layoutDiv;
}
function drncdPostGroupCreateTextPost( news_data ) {
    var layoutDiv = $("<div class=\'col-lg-6 col-md-6 col-sm-12 col-xs-12\' />");

    var linkObj = $("<a />");
    linkObj.attr({ href : news_data.url });

    var innerLayoutDiv = $("<div class=\'news row\' />");

    var textDiv = $("<div class=\'col-lg-12 col-md-12 col-sm-12 col-xs-12 news-text\' />");
    var dateDiv = $("<div class=\'news-fulldate\'>" + news_data.post_date + "</div>");
    if ( 
        news_data.date_color !== undefined && 
        news_data.date_color != null &&
        news_data.date_color != ""
        ) {
        dateDiv.css("color", news_data.date_color);
    }
    dateDiv.appendTo(textDiv);
    var titleDiv = $("<div class=\'news-fulltitle\'>" + news_data.post_title + "</div>");
    if ( 
        news_data.title_color !== undefined && 
        news_data.title_color != null &&
        news_data.title_color != ""
        ) {
        titleDiv.css("color", news_data.title_color);
    }
    titleDiv.appendTo(textDiv);
    if ( news_data.background_color !== undefined && news_data.background_color != null && news_data.background_color != "" ) {
        textDiv.css("background-color", news_data.background_color);
    } else {
        textDiv.css("background-color", drncdPostDefaultBackgroundColor);
    }
    textDiv.appendTo(innerLayoutDiv);

    innerLayoutDiv.appendTo(linkObj);
    linkObj.appendTo(layoutDiv);

    return layoutDiv;
}

