var drncd_data_footer = {
    "custom-service-tel" : "801-800-8888",
    "hot-line" : "021-88886666",
    "icp" : "沪ICP备00000000号-12",
    "social-accounts": [
        {
            "name" : "Facebook",
            "account" : "drncd_facebook",
            "link" : "https://www.facebook.com/user/drncd_facebook",
            "icon" : "assets/images/icon_facebook.png"
        },
        {
            "name" : "WeChat",
            "account" : "drncd_wechat",
            "link" : "https://wx.qq.com/user/drncd_wechat",
            "icon" : "assets/images/icon_wechat.png"
        },
        {
            "name" : "Weibo",
            "account" : "drncd_weibo",
            "link" : "https://weibo.com/user/drncd_weibo",
            "icon" : "assets/images/icon_weibo.png"
        },
        {
            "name" : "Twitter",
            "account" : "drncd_twitter",
            "link" : "https://twitter.com/user/drncd_twitter",
            "icon" : "assets/images/icon_twitter.png"
        },
        {
            "name" : "Tmall",
            "account" : "drncd_tmall",
            "link" : "https://www.tmall.com/user/drncd_tmall",
            "icon" : "assets/images/icon_tmall.png"
        },
        {
            "name" : "Instgram",
            "account" : "drncd_ins",
            "link" : "https://www.ins.com/user/drncd_ins",
            "icon" : "assets/images/icon_ins.png"
        }
    ]
};