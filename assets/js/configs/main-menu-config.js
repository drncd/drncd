var drncd_data_menu = [
    {
        "item" : "关于Dr.NCD",
        "url" : "#",
        "target" : "",
        "image" : "",
        "children" : [
            {
                "item" : "品牌故事",
                "url" : "#company",
                "target" : "",
                "image" : "assets/images/menu_template_image.png"
            },
            {
                "item" : "品牌发展",
                "url" : "#company",
                "target" : "",
                "image" : "assets/images/menu_template_image.png"
            },
            {
                "item" : "专柜查询",
                "url" : "/stores/",
                "target" : "",
                "image" : "assets/images/menu_template_image.png"
            }
        ]
    }, 
    {
        "item" : "全线产品",
        "url" : "#",
        "target" : "",
        "image" : "",
        "children" : [
            {
                "item" : "新品上市",
                "url" : "#company",
                "target" : "",
                "image" : "assets/images/product_big.png"
            },
            {
                "item" : "明星单品",
                "url" : "#company",
                "target" : "",
                "image" : "assets/images/menu_template_image.png"
            },
            {
                "item" : "产品系列",
                "url" : "/stores/",
                "target" : "",
                "image" : "assets/images/menu_template_image.png",
                "children": [
                    {
                        "item" : "分类1",
                        "url" : "#company",
                        "target" : "",
                        "image" : "assets/images/menu_template_image.png"
                    },
                    {
                        "item" : "分类2",
                        "url" : "#company",
                        "target" : "",
                        "image" : "assets/images/menu_template_image.png"
                    },
                    {
                        "item" : "分类3",
                        "url" : "#company",
                        "target" : "",
                        "image" : "assets/images/menu_template_image.png"
                    },
                    {
                        "item" : "分类4",
                        "url" : "#company",
                        "target" : "",
                        "image" : "assets/images/menu_template_image.png"
                    },
                    {
                        "item" : "分类5",
                        "url" : "#company",
                        "target" : "",
                        "image" : "assets/images/menu_template_image.png"
                    },
                    {
                        "item" : "分类6",
                        "url" : "#company",
                        "target" : "",
                        "image" : "assets/images/menu_template_image.png"
                    }
                ]
            },
            {
                "item" : "使用步骤",
                "url" : "#company",
                "target" : "",
                "image" : "assets/images/menu_template_image.png"
            },
            {
                "item" : "本月推荐",
                "url" : "#company",
                "target" : "",
                "image" : "assets/images/menu_template_image.png"
            }
        ]
    },
    {
        "item" : "尊享套装",
        "url" : "#",
        "target" : "",
        "image" : ""
    },
    {
        "item" : "专家建议",
        "url" : "#",
        "target" : "",
        "image" : ""
    },
    {
        "item" : "新闻报道",
        "url" : "#",
        "target" : "",
        "image" : ""
    },
    {
        "item" : "会员专享",
        "url" : "#",
        "target" : "",
        "image" : "",
        "children" : [
            {
                "item" : "积分兑换",
                "url" : "#company",
                "target" : "",
                "image" : "assets/images/menu_template_image.png"
            },
            {
                "item" : "会员活动",
                "url" : "#company",
                "target" : "",
                "image" : "assets/images/menu_template_image.png"
            }
        ]
    }
];
