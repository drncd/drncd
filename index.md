---
# You don't need to edit this file, it's empty on purpose.
# Edit theme's home layout instead if you wanna make some changes
# See: https://jekyllrb.com/docs/themes/#overriding-theme-defaults
layout: drncd-main-style
title: 首页
---

{% include banner.html %}
{% include company.html %}
{% include hotnews.html %}
{% include topproduct.html %}
<script>
    function pageInitialize() {
        drncdBannerInitialize();
        drncdCompanyInitialize();
        drncdHotNewsInititalize();
        drncdTopProductInitialize();
    }
</script>